#!/usr/bin/sh

if [ -z "$1" ]
then
  echo "Please provide app name"
  exit 1
fi

npx -y create-next-app "$1" --typescript --use-npm
cd "$1"
npm i @chakra-ui/react @emotion/react@^11 @emotion/styled@^11 framer-motion@^4 @chakra-ui/icons
npm i -D prettier

cat > ./pages/_app.tsx <<- EOF
import '../styles/globals.css'
import { ChakraProvider } from "@chakra/react"
import type { AppProps } from 'next/app'

function MyApp({ Component, pageProps }: AppProps) {
  return <ChakraProvider>
    <Component {...pageProps} />
  </ChakraProvider>
}
export default MyApp
EOF

npx -y prettier -w ./pages/_app.tsx

echo "semi: false" > ./.prettierrc.yml
